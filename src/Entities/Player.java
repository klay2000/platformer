package Entities;

import Physics.Universe;
import Sound.SoundPlayer;
import graphics.Quad;
import graphics.Window;
import main.GameObject;
import main.KeyboardHandler;
import main.Resources;
import org.dyn4j.dynamics.Body;
import org.dyn4j.dynamics.contact.ContactPoint;
import org.dyn4j.geometry.Circle;
import org.dyn4j.geometry.MassType;
import org.dyn4j.geometry.Vector2;

import static org.lwjgl.glfw.GLFW.*;

public class Player extends GameObject {

    private KeyboardHandler keyHandler; // keyHandler for getting pressed keys

    private int jumps = 0; // how many times the player has jumped since touching something
    private int maxJumps = 0; // maximum jumps without touching something
    private boolean jumpHeld = true; // is the jump button held
    private float lastJumpTime = 0; // time of last jump
    private float jumpForce = 10000; // force of jump
    private float jumpForceMultiplier = 2; // number jumpForce is multiplied by when makeBounce() is called

    private float spinForce = 2000; // torque player has when moving
    private float spinForceMultiplier = 2; // what torque is multiplied by when makeFast() is called

    private float frictionMultiplier = 4; // what friction is multiplied by when makeFriction() is called

    private boolean bounce = false; // is player bouncy
    private boolean friction = false; // is player friction-y
    private boolean fast = false; // is player fast

    private boolean isAlive = true;

    private float spawnX; // spawn x of player
    private float spawnY; // spawn y of player
    private float scaleFactor; // scaleFactor of players universe

    private SoundPlayer soundPlayer;

    public float levelWidth; // width of current level

    private Universe universe; // players universe

    Window window; // window handle

    public Player(int maxJumps, float scaleFactor, Universe universe, float spawnX, float spawnY, float rot, Window window, KeyboardHandler keyHandler, SoundPlayer soundPlayer){
        this.spawnX = spawnX; // sets spawn x
        this.spawnY = spawnY; // sets spawn y

        this.universe = universe; // sets universe

        this.scaleFactor = scaleFactor; // sets scaleFactor of universe

        this.window = window; // sets window

        this.keyHandler = keyHandler; // sets keyhandler

        this.soundPlayer = soundPlayer;

        this.maxJumps = maxJumps; // sets maxJumps

        body = new Body(); // initializes body of gameObject

        body.addFixture(new Circle(50*scaleFactor), 0.1, 0.5, 0.3); // adds fixture

        body.setAngularDamping(1);

        body.setMass(MassType.NORMAL); // sets mass to normal

        body.setUserData("player"); // sets UserData of player body

        body.translate(spawnX*scaleFactor, spawnY*scaleFactor); // translates body to x and y
        body.rotate(Math.toDegrees(rot)); // rotates body

        model = new Quad(0, 0, 50*2*scaleFactor, 50*2*scaleFactor, Resources.whiteCircle); // creates model

        universe.addBody(body); // adds body to universe
    }

    @Override
    public void tick() {
        if (keyHandler.getKey(GLFW_KEY_D)) applyTorque(-spinForce); // if d is down spin clockwise
        if (keyHandler.getKey(GLFW_KEY_A)) applyTorque(spinForce); // if a is down spin counterclockwise
        if (keyHandler.getKey(GLFW_KEY_W) && jumps < maxJumps && !jumpHeld && glfwGetTime() - lastJumpTime > 0.3) { // if w is down jump
            applyForce(new Vector2(0, jumpForce));
            jumps++;
            jumpHeld = true;
            lastJumpTime = (float) glfwGetTime();
        }
        if (!keyHandler.getKey(GLFW_KEY_W)) jumpHeld = false; // if w isn't down set jumpHeld to false

        for (ContactPoint contact : universe.getContacts(body)) { // iterates through all collisions with player
            jumps = 0; // sets jumps since last collision to 0
            Body collided; // makes collided
            if (contact.getBody1().equals(body)) collided = contact.getBody2(); // if player is body1 collided is body2
            else collided = contact.getBody1(); // else collided is body1

//            if(Math.random() > .5)soundPlayer.play(Resources.circleCollide); // plays circle collision noise

            switch ((String) collided.getUserData()) {
                case "kill": // if collided's UserData is kill then player is killed
                    isAlive = false;
                    break;
                case "teleport": // if collided's UserData is teleport then player is teleported to spawn and the camera is reset
                    body.getTransform().setTranslation(spawnX * scaleFactor, spawnY * scaleFactor);
                    window.camera.translate(-window.camera.getX(), -window.camera.getY());
                    break;
                case "clearEffects": // if collided's UserData is clearEffects then clear all effects
                    clearEffects();
                    break;
                case "makeSpeed": // if collided's UserData is makeSpeed then make player fast
                    makeFast();
                    break;
                case "makeFriction": // if collided's UserData is makeFriction then make player friction-y
                    makeFriction();
                    break;
                case "makeBounce": // if collided's UserData is make Bounce then make player bouncy
                    makeBouncy();
                    break;
            }

        }

        //adjust camera to stay on player
        float width = window.getWidth();
        float height = window.getHeight();

        float camX = -window.camera.getX();
        float camY = -window.camera.getY();

        float playX = (float) getPosition().x;
        float playY = (float) getPosition().y;

        float xThreshold = 16;
        float yThreshold = 30;

        float yOffset = height/18;

        float xTrans = (playX) - camX - ((width / 2) * scaleFactor);
        float yTrans = (playY - camY - (yOffset * scaleFactor));

        if (Math.abs(xTrans) > xThreshold) {
            if ((camX/scaleFactor)+(width)+xTrans<levelWidth) {
                if (xTrans > 0) {
                    if (((camX) + (width)) + ((xTrans / scaleFactor) - xThreshold) < levelWidth)
                        window.camera.translate(-(xTrans - xThreshold), 0);
                } else {
                    if ((camX * scaleFactor) + (xTrans + xThreshold) > 0)
                        window.camera.translate(-(xTrans + xThreshold), 0);
                }
            }else window.camera.translate(camX-(levelWidth-width)*scaleFactor, 0);
        }

        if (yTrans > yThreshold)window.camera.translate(0, -yTrans + yThreshold);

        if (camY - yTrans > 0 && yTrans < 0) window.camera.translate(0, -yTrans);
    }


    public void makeFriction(){ // makes player friction-y
        if(!friction) {
            body.getFixture(0).setFriction(body.getFixture(0).getFriction() * frictionMultiplier);
            friction = true;
        }
    }

    public void makeBouncy(){ // makes player bouncy
        if(!bounce) {
            jumpForce *= jumpForceMultiplier;
            bounce = true;
        }
    }

    public void makeFast(){ // makes player fast
        if(!fast) {
            spinForce *= spinForceMultiplier;
            fast = true;
        }
    }

    public void clearEffects(){ // clears players effects
        if(fast) spinForce /= spinForceMultiplier;
        if(bounce) jumpForce /= jumpForceMultiplier;
        if(friction) body.getFixture(0).setFriction(body.getFixture(0).getFriction()/frictionMultiplier);

        fast = false;
        bounce = false;
        friction = false;
    }

    public boolean getIsAlive(){
        return isAlive;
    }

}
