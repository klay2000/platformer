package Entities;

import Physics.Universe;
import graphics.Quad;
import graphics.Texture;
import main.GameObject;
import main.Resources;
import org.dyn4j.dynamics.Body;
import org.dyn4j.geometry.MassType;
import org.dyn4j.geometry.Rectangle;

import java.util.Scanner;

public class Wall extends GameObject{

    float frictionMultiplier = 1; // what bodies friction is multiplied by when makeFriction() is called

    public Wall(float x, float y, float width, float height, float rot, Texture texture, float restitution, float friction, float scaleFactor, Universe universe){
        GameObject gameObject = new GameObject(); // initializes gameObject

        body = new Body(); // initializes body of gameObject

        body.addFixture(new Rectangle(width * scaleFactor, height * scaleFactor), 1, friction, restitution); // adds fixture

        body.setMass(MassType.INFINITE); // make mass infinite

        body.rotate(Math.toRadians(rot)); // rotates body
        body.translate(x*scaleFactor, y*scaleFactor); // translates body to x and y

        body.setUserData("wall"); // sets UserData to wall

        model = new Quad(0, 0, width*scaleFactor, height*scaleFactor, texture); // creates model

        universe.addBody(body); // adds body to universe
    }

    public static Wall makeWhiteWall(float x, float y, float width, float height, float rot, float scaleFactor, Universe universe){ // makes a normal white wall
        Wall wall = new Wall(x, y, width, height, rot, Resources.whiteWall, 0.25f, 0.25f, scaleFactor, universe);
        return wall;
    }

    public static Wall makeRedWall(float x, float y, float width, float height, float rot, float scaleFactor, Universe universe){ // makes a killer wall
        Wall wall = new Wall(x, y, width, height, rot, Resources.redWall, 0.25f, 0.25f, scaleFactor, universe);

        wall.body.setUserData("kill");

        return wall;
    }

    public static Wall makeGreenWall(float x, float y, float width, float height, float rot, float scaleFactor, Universe universe){ // makes a wall that teleports player to spawn
        Wall wall = new Wall(x, y, width, height, rot, Resources.greenWall, 0.25f, 0.25f, scaleFactor, universe);

        wall.body.setUserData("teleport");

        return wall;
    }

    public static Wall makeBlueWall(float x, float y, float width, float height, float rot, float scaleFactor, Universe universe){ // makes a wall that removes effects
        Wall wall = new Wall(x, y, width, height, rot, Resources.blueWall, 0.25f, 0.25f, scaleFactor, universe);

        wall.body.setUserData("clearEffects");

        return wall;
    }

    public static Wall makeCyanWall(float x, float y, float width, float height, float rot, float scaleFactor, Universe universe){ // makes a wall that makes player fast
        Wall wall = new Wall(x, y, width, height, rot, Resources.cyanWall, 0.25f, 0.25f, scaleFactor, universe);

        wall.body.setUserData("makeSpeed");

        return wall;
    }

    public static Wall makeMagentaWall(float x, float y, float width, float height, float rot, float scaleFactor, Universe universe){ // makes a wall that increases player friction
        Wall wall = new Wall(x, y, width, height, rot, Resources.magentaWall, 0.25f, 0.25f, scaleFactor, universe);

        wall.body.setUserData("makeFriction");

        wall.makeFriction();

        return wall;
    }

    public static Wall makeYellowWall(float x, float y, float width, float height, float rot, float scaleFactor, Universe universe){ // makes a wall that makes player bouncy
        Wall wall = new Wall(x, y, width, height, rot, Resources.yellowWall, 0.25f, 0.25f, scaleFactor, universe);

        wall.body.setUserData("makeBounce");

        wall.makeBouncy();

        return wall;
    }

    private void makeFriction(){ // makes wall friction-y
        body.getFixture(0).setFriction(body.getFixture(0).getFriction()*frictionMultiplier);
    }

    private void makeBouncy(){ // makes wall bouncy
        body.getFixture(0).setRestitution(1);
    }

}
