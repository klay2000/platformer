package Entities;

import Physics.Universe;
import Sound.SoundPlayer;
import graphics.Quad;
import graphics.Texture;
import main.GameObject;
import main.Resources;
import org.dyn4j.dynamics.Body;
import org.dyn4j.dynamics.contact.ContactPoint;
import org.dyn4j.geometry.MassType;

public class Circle extends GameObject {

    private static float frictionMultiplier = 10; // what friction is multiplied by when makeFriction() is called
    private static float densityMultiplier = 5; // what density is multiplied by when makeFast() is called

    private static float defaultRestitution = 0.25f; // default restitution

    private SoundPlayer soundPlayer;

    private boolean isSpecialized = true; // whether the circle has any sort of specialization or is just white

    private Universe universe; // the universe that the circle is in

    public Circle(float x, float y, float radius, float density, float friction, float restitution, float scaleFactor, Universe universe, SoundPlayer soundPlayer, Texture texture) {
        body = new Body(); // initializes body of gameObject

        body.addFixture(new org.dyn4j.geometry.Circle(radius * scaleFactor), density, friction, restitution); // adds fixture

        body.setMass(MassType.NORMAL); // sets mass to normal

        body.translate(x * scaleFactor, y * scaleFactor); // translates body to x and y

        model = new Quad(0, 0, radius * 2 * scaleFactor, radius * 2 * scaleFactor, texture); // makes model

        this.soundPlayer = soundPlayer;

        universe.addBody(body); // adds body to universe

        this.universe = universe; // sets universe equal to universe inputted
    }

    public static Circle makeWhiteCircle(float x, float y, float scaleFactor, SoundPlayer soundPlayer, Universe universe) { // makes a white un-specialized circle
        Circle circle = new Circle(x, y, 10, 1, 1, defaultRestitution, scaleFactor, universe, soundPlayer, Resources.whiteCircle);

        circle.body.setUserData("");

        circle.isSpecialized = false;

        return circle;
    }

    public static Circle makeRedCircle(float x, float y, float scaleFactor, SoundPlayer soundPlayer, Universe universe) { // makes killer circle
        Circle circle = new Circle(x, y, 10, 1, 1, defaultRestitution, scaleFactor, universe, soundPlayer, Resources.redCircle);

        circle.body.setUserData("kill");

        return circle;
    }

    public static Circle makeGreenCircle(float x, float y, float scaleFactor, SoundPlayer soundPlayer, Universe universe) { // makes circle that teleports you to spawn
        Circle circle = new Circle(x, y, 10, 1, 1, defaultRestitution, scaleFactor, universe, soundPlayer, Resources.greenCircle);

        circle.body.setUserData("teleport");

        return circle;
    }

    public static Circle makeBlueCircle(float x, float y, float scaleFactor, SoundPlayer soundPlayer, Universe universe) { // makes circle that removes your abilities
        Circle circle = new Circle(x, y, 10, 1, 1, defaultRestitution, scaleFactor, universe, soundPlayer, Resources.blueCircle);

        circle.body.setUserData("clearEffects");

        return circle;
    }

    public static Circle makeCyanCircle(float x, float y, float scaleFactor, SoundPlayer soundPlayer, Universe universe) { // makes circle that makes you go fast
        Circle circle = new Circle(x, y, 10, 1, 1, defaultRestitution, scaleFactor, universe, soundPlayer, Resources.cyanCircle);

        circle.body.setUserData("makeSpeed");

        circle.makeFast();

        return circle;
    }

    public static Circle makeMagentaCircle(float x, float y, float scaleFactor, SoundPlayer soundPlayer, Universe universe) { // makes circle that makes you friction-y
        Circle circle = new Circle(x, y, 10, 1, 1, defaultRestitution, scaleFactor, universe, soundPlayer, Resources.magentaCircle);

        circle.makeFriction();

        circle.body.setUserData("makeFriction");

        return circle;
    }

    public static Circle makeYellowCircle(float x, float y, float scaleFactor, SoundPlayer soundPlayer, Universe universe) { // makes circle that makes you bouncy
        Circle circle = new Circle(x, y, 10, 1, 1, defaultRestitution, scaleFactor, universe, soundPlayer, Resources.yellowCircle);

        circle.makeBouncy();

        circle.body.setUserData("makeBounce");

        return circle;
    }

    @Override
    public void tick(){ // allows the a circle to become specialized if it is un-specialized
        if(!isSpecialized) {
            for (ContactPoint contact : universe.getContacts(body)) {
                Body collided;
                if (contact.getBody1().equals(body)) collided = contact.getBody2();
                else collided = contact.getBody1();

                if(Math.random() > .5)soundPlayer.play(Resources.circleCollide); // plays collision noise

                switch ((String) collided.getUserData()) {
                    case "kill":
                            changeTexture(Resources.redCircle);

                            body.setUserData("kill");
                            isSpecialized = true;
                        break;
                    case "teleport":
                            changeTexture(Resources.greenCircle);

                            body.setUserData("teleport");
                            isSpecialized = true;
                        break;
                    case "clearEffects":
                            changeTexture(Resources.blueCircle);

                            body.setUserData("clearEffects");
                            isSpecialized = true;
                        break;
                    case "makeSpeed":
                            changeTexture(Resources.cyanCircle);

                            body.setUserData("makeSpeed");
                            makeFast();
                            isSpecialized = true;
                        break;
                    case "makeFriction":
                            changeTexture(Resources.magentaCircle);

                            body.setUserData("makeFriction");
                            makeFriction();
                            isSpecialized = true;
                        break;
                    case "makeBounce":
                            changeTexture(Resources.yellowCircle);

                            body.setUserData("makeBounce");
                            makeBouncy();
                            isSpecialized = true;
                        break;
                }
                if(isSpecialized) {
                    if(universe.getContacts(body).size() > 0) soundPlayer.play(Resources.circleCollide); // plays collision noise
                    break; // this prevents a circle from being specialized twice
                }
            }
        }
}

    private void makeFriction(){ // makes a circle friction-y
        body.getFixture(0).setFriction(body.getFixture(0).getFriction()*frictionMultiplier); //
    }

    private void makeBouncy(){ // makes a circle bouncy
        body.getFixture(0).setRestitution(1);
    }

    private void makeFast(){ // makes a circle fast (increases density)
        body.getFixture(0).setDensity(body.getFixture(0).getDensity()*densityMultiplier);
    }
}
