package graphics;

import org.joml.Matrix4f;
import org.lwjgl.BufferUtils;
import org.lwjgl.opengl.GL20;

import java.io.*;
import java.nio.FloatBuffer;

import static org.lwjgl.opengl.GL20.*;
public class Shader {
    private int program; // shader program id
    private int vs; // vertex shader id
    private int fs; // fragment shader id

    public Shader(String filename){ // creates new shader from filename of sources
        program = glCreateProgram(); // creates program and sets id

        vs = glCreateShader(GL_VERTEX_SHADER); // creates vertex shader and sets id
        glShaderSource(vs, readFile(filename+".vs")); // loads vertex shader source
        glCompileShader(vs); // compiles vertex shader
        if(glGetShaderi(vs, GL_COMPILE_STATUS)!= 1)System.err.println(glGetShaderInfoLog(vs)); // prints errors if they exist

        fs = glCreateShader(GL_FRAGMENT_SHADER); // creates fragment shader
        glShaderSource(fs, readFile(filename+".fs")); // loads fragment shader source
        glCompileShader(fs); // compiles fragment shader
        if(glGetShaderi(fs, GL_COMPILE_STATUS)!= 1)System.err.println(glGetShaderInfoLog(vs)); // prints errors if they exist

        glAttachShader(program, vs); // attaches vertex shader
        glAttachShader(program, fs); // attaches fragment shader

        glBindAttribLocation(program, 0, "vertices"); // binds vertices attribute
        glBindAttribLocation(program, 1, "textures"); // binds textures attribute

        glLinkProgram(program); // links program
        if(glGetProgrami(program, GL_LINK_STATUS)!= 1)System.err.println(glGetProgramInfoLog(program)); // prints errors if they exist

        glValidateProgram(program); // validates program
        if(glGetProgrami(program, GL_VALIDATE_STATUS)!= 1)System.err.println(glGetProgramInfoLog(program)); // prints errors if they exist
    }

    protected void setUniform(String name, int value){ // sets the value of a uniform int
        int location = glGetUniformLocation(program, name);
        if(location!=-1)
            glUniform1i(location, value);
        else System.err.println("invalid uniform");
    }

    protected void setUniform(String name, Matrix4f matrix){ // sets the value of a uniform matrix4f
        int location = glGetUniformLocation(program, name);
        FloatBuffer buffer = BufferUtils.createFloatBuffer(16);
        matrix.get(buffer);
        if(location!=-1)
            GL20.glUniformMatrix4fv(location, false, buffer);
        else System.err.println("invalid uniform");
    }

    protected void bind(){ // binds shader
        glUseProgram(program);
    }

    private String readFile(String filename){ // converts file to string
        StringBuilder string = new StringBuilder();
        BufferedReader br;
        try {
            br = new BufferedReader(new FileReader(new File("./shaders/"+filename)));
            String line;
            while ((line = br.readLine()) != null){
                string.append(line);
                string.append("\n");
            }

            br.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return string.toString();

    }

}
