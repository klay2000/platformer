package graphics;

import org.joml.Matrix4f;
import org.joml.Vector3f;

public class Camera {

    private Matrix4f cameraMatrix; // matrix passed to shader for camera modifications

    private float x = 0; // x position
    private float y = 0; // y position
    private float rot = 0; // rotation

    public Camera(float x, float y, float width, float height){ // creates new camera from x pos, y pos, width, and height

        cameraMatrix = new Matrix4f(); // initializes matrix
        cameraMatrix.scale(1/(width/2), 1/(height/2), 1); // scales camera to screen size
        cameraMatrix.translate(-x, -y, 0); // translates camera
    }

    public void zoom(float zoomFactor){ // zooms camera
        cameraMatrix.scale(zoomFactor);
    }

    public void translate(float x, float y){ // translates camera
        cameraMatrix.translate(x, y, 0);
        this.x+=x;
        this.y+=y;
    }

    public void rotate(float degrees){ // rotates camera
        cameraMatrix.rotate((float) Math.toRadians(degrees), new Vector3f(0, 0, 1));
        rot += degrees;
    }

    public Matrix4f getMatrix(){ // returns cameras matrix
        return cameraMatrix;
    }

    public float getX() { // returns x coordinate
        return x;
    }

    public float getY() { // returns y coordinate
        return y;
    }

    public float getRotation() { // returns rotation
        return rot;
    }
}
