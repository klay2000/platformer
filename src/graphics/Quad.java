package graphics;

public class Quad extends Model{
    public Quad(float x, float y, float width, float height, Texture texture){ // makes a quad from its position, dimensions, and texture
        super(new float[]{
                x-(width/2), y+(height/2), 0,
                x+(width/2), y+(height/2), 0,
                x+(width/2), y-(height/2), 0,
                x-(width/2), y-(height/2), 0
        },new float[]{0, 0, 1, 0, 1, 1, 0, 1}, new int[]{0, 1, 2, 2, 3, 0}, texture);
    }
}
