package Physics;

import org.dyn4j.dynamics.Body;
import org.dyn4j.dynamics.contact.ContactPoint;
import org.dyn4j.dynamics.contact.PersistedContactPoint;
import org.dyn4j.dynamics.contact.SolvedContactPoint;

import java.util.ArrayList;
import java.util.List;

public class ContactListener implements org.dyn4j.dynamics.contact.ContactListener {

    public ArrayList<ContactPoint> contacts = new ArrayList<>();

    @Override
    public void sensed(ContactPoint contactPoint) {

    }

    @Override
    public boolean begin(ContactPoint contactPoint) {
        contacts.add(contactPoint);
        return true;
    }

    @Override
    public void end(ContactPoint contactPoint) {

    }

    @Override
    public boolean persist(PersistedContactPoint persistedContactPoint) {
        return true;
    }

    @Override
    public boolean preSolve(ContactPoint contactPoint) {
        return true;
    }

    @Override
    public void postSolve(SolvedContactPoint solvedContactPoint) {
    }

    public void reset(){
        contacts.clear();
    }

    public ArrayList<ContactPoint> getContacts(Body body){
        ArrayList<ContactPoint> list = new ArrayList();
        for(ContactPoint contact : contacts) {
            if(contact.getBody1().equals(body) || contact.getBody2().equals(body))list.add(contact);
        }
        return list;
    }

}
