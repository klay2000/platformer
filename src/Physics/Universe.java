package Physics;

import org.dyn4j.dynamics.Body;
import org.dyn4j.dynamics.World;
import org.dyn4j.dynamics.contact.Contact;
import org.dyn4j.dynamics.contact.ContactPoint;
import org.dyn4j.geometry.Vector2;

import java.util.ArrayList;

public class Universe {

    protected World world;
    protected ContactListener contactListener;

    public Universe(Vector2 gravity){
        world = new World();
        world.setGravity(gravity);

        contactListener = new ContactListener();
        world.addListener(contactListener);
    }

    public void addBody(Body body){
        world.addBody(body);
    }

    public ArrayList<ContactPoint> getContacts(Body body){
        return contactListener.getContacts(body);
    }

    public void step(int steps){
        contactListener.reset();
        world.step(steps);
    }

    public void setGravity(Vector2 gravity){
        world.setGravity(gravity);
    }

}
