package Sound;

import org.lwjgl.openal.AL;
import org.lwjgl.openal.ALC;
import org.lwjgl.openal.ALCCapabilities;
import org.lwjgl.openal.ALCapabilities;

import java.nio.IntBuffer;
import java.nio.ShortBuffer;

import static org.lwjgl.openal.AL10.*;
import static org.lwjgl.openal.AL10.alSourcePlay;
import static org.lwjgl.openal.ALC10.*;
import static org.lwjgl.openal.ALC10.alcMakeContextCurrent;
import static org.lwjgl.stb.STBVorbis.stb_vorbis_decode_filename;
import static org.lwjgl.system.MemoryStack.stackMallocInt;
import static org.lwjgl.system.MemoryStack.stackPop;
import static org.lwjgl.system.MemoryStack.stackPush;
import static org.lwjgl.system.libc.LibCStdlib.free;

public class SoundPlayer {


    String defaultDeviceName; // name of default audio device
    long device; // id of audio device
    int[] attributes = {0}; // array of audio device attributes
    long context; // openAL context
    ALCCapabilities alcCapabilities; // allows OpenAL to do stuff
    ALCapabilities alCapabilities; // allows OpenAL to do stuff


    public SoundPlayer(){

        //OpenAL Test

        defaultDeviceName = alcGetString(0, ALC_DEFAULT_DEVICE_SPECIFIER); // sets default device name
        device = alcOpenDevice(defaultDeviceName); // sets device
        context = alcCreateContext(device, attributes); // sets OpenAL context
        alcMakeContextCurrent(context); // sets up openAL context
        alcCapabilities = ALC.createCapabilities(device); // creates capabilities for OpenAL
        alCapabilities = AL.createCapabilities(alcCapabilities); // creates capabilities for OpenAL

    }

    public void cleanUp(){ // closes everything to clean up after
        alcDestroyContext(context); // destroys al context
        alcCloseDevice(device); // closes al device
    }

    public void play(Sound sound){ // plays a sound

        alSourcePlay(sound.sourcePointer);

    }

}
