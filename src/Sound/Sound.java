package Sound;

import java.nio.IntBuffer;
import java.nio.ShortBuffer;

import static org.lwjgl.openal.AL10.*;
import static org.lwjgl.openal.AL10.alSourcePlay;
import static org.lwjgl.stb.STBVorbis.stb_vorbis_decode_filename;
import static org.lwjgl.system.MemoryStack.stackMallocInt;
import static org.lwjgl.system.MemoryStack.stackPop;
import static org.lwjgl.system.MemoryStack.stackPush;
import static org.lwjgl.system.libc.LibCStdlib.free;

public class Sound {

    protected int sourcePointer;

    private int length;

    public Sound(String path, int length){ // makes sound

        this.length = length; // length of sound

        stackPush(); // pushes stack
        IntBuffer channelsBuffer = stackMallocInt(1); // creates channel buffer
        stackPush(); // pushes stack
        IntBuffer sampleRateBuffer = stackMallocInt(1); // creates sample rate buffer

        ShortBuffer rawAudioBuffer = stb_vorbis_decode_filename(path, channelsBuffer, sampleRateBuffer); // decodes song to make raw buffer

        int channels = channelsBuffer.get(); // gets number of channels

        int sampleRate = sampleRateBuffer.get(); // gets sampleRate

        stackPop(); // pops stack
        stackPop(); // pops stack

        // mono or stereo

        int format = -1;

        if(channels == 1){
            format = AL_FORMAT_MONO16;
        }else if(channels == 2){
            format = AL_FORMAT_STEREO16;
        }

        int bufferPointer = alGenBuffers(); // makes pointer to buffers

        alBufferData(bufferPointer, format, rawAudioBuffer, sampleRate); // sets buffer data

        free(rawAudioBuffer); // frees buffer

        sourcePointer = alGenSources(); // creates source pointer

        alSourcei(sourcePointer, AL_BUFFER, bufferPointer); // sets up source
    }

    public int getLength() { // returns length of audio
        return length;
    }
}
