package Sound;

import main.Resources;

public class MusicPlayer implements Runnable{ // this is a thread that plays music from the soundtrack

    SoundPlayer soundPlayer;

    public MusicPlayer(SoundPlayer soundPlayer){
        this.soundPlayer = soundPlayer;
    }

    @Override
    public void run() {

        while(true){

            int song = (int)Math.floor(Math.random()*Resources.music.size()); // chooses song

            soundPlayer.play(Resources.music.get(song)); // plays song

            try {
                Thread.sleep(Resources.music.get(song).getLength()*1000); // waits for end of song
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

        }

    }
}
