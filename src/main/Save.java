package main;

import java.io.*;

public class Save implements Serializable{

    int level = 1; // level player is at
    int lives = 3; // lives player has

    public void load( String filename ){ // loads from save file
        try {

            Save save = (Save) new ObjectInputStream(new FileInputStream(filename)).readObject();

            this.level = save.level;
            this.lives = save.lives;

        } catch (IOException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
    }

    public void save( String filename ){ // saves to save file
        try {

            new ObjectOutputStream(new FileOutputStream(filename)).writeObject(this);

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
