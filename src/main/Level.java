package main;

import Entities.Circle;
import Entities.Player;
import Entities.Wall;
import Physics.Universe;
import Sound.SoundPlayer;
import graphics.Window;
import org.dyn4j.geometry.Vector2;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import java.io.File;
import java.util.ArrayList;

import static org.lwjgl.glfw.GLFW.glfwGetTime;

public class Level extends DefaultHandler {

    private ArrayList<GameObject> gameObjects; // contains all gameobjects in the level

    private Universe universe; // the levels universe

    private float scaleFactor; // the levels scalefactor
    private Window window; // the window handle
    private KeyboardHandler keyHandler; // the keyhandler

    private int playerIndex; // the index of the player in the arraylist

    private float levelWidth; // width of the level, once the player passes this x position they win
    private boolean isLevelPassed = false; // true once level is passed

    private String path; // path to the levels xml file

    private SoundPlayer soundPlayer;

    float lastPhysUpdate = (float) glfwGetTime();
    float steps = 0;
    float accumSteps = 0;

    public Level(String path, float scaleFactor, Window window, KeyboardHandler keyHandler, SoundPlayer soundPlayer) { // constructor for level
        try {

            this.soundPlayer = soundPlayer;

            gameObjects = new ArrayList<>();

            this.path = path;

            this.scaleFactor = scaleFactor;
            this.window = window;
            this.keyHandler = keyHandler;

            window.camera.translate(-window.camera.getX(), -window.camera.getY());

            File level = new File(path);

            SAXParserFactory factory = SAXParserFactory.newInstance();

            SAXParser sax = factory.newSAXParser();

            sax.parse(level, this);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void startElement(String uri, String localName, String qName, Attributes attributes) throws SAXException { // this is called when an xml element is read

        float x; // x position
        float y; // y position
        float width; // width
        float height; // height
        float rot; // rotation

        switch (qName){

            case "whiteWall": // makes a white wall
                x = Float.parseFloat(attributes.getValue("x"));
                y = Float.parseFloat(attributes.getValue("y"));
                width = Float.parseFloat(attributes.getValue("width"));
                height = Float.parseFloat(attributes.getValue("height"));
                rot = Float.parseFloat(attributes.getValue("rot"));
                gameObjects.add(Wall.makeWhiteWall(x, y, width, height, rot, scaleFactor, universe));
                break;

            case "redWall": // makes a red wall
                x = Float.parseFloat(attributes.getValue("x"));
                y = Float.parseFloat(attributes.getValue("y"));
                width = Float.parseFloat(attributes.getValue("width"));
                height = Float.parseFloat(attributes.getValue("height"));
                rot = Float.parseFloat(attributes.getValue("rot"));
                gameObjects.add(Wall.makeRedWall(x, y, width, height, rot, scaleFactor, universe));
                break;

            case "greenWall": // makes a green wall
                x = Float.parseFloat(attributes.getValue("x"));
                y = Float.parseFloat(attributes.getValue("y"));
                width = Float.parseFloat(attributes.getValue("width"));
                height = Float.parseFloat(attributes.getValue("height"));
                rot = Float.parseFloat(attributes.getValue("rot"));
                gameObjects.add(Wall.makeGreenWall(x, y, width, height, rot, scaleFactor, universe));
                break;

            case "blueWall": // makes a blue wall
                x = Float.parseFloat(attributes.getValue("x"));
                y = Float.parseFloat(attributes.getValue("y"));
                width = Float.parseFloat(attributes.getValue("width"));
                height = Float.parseFloat(attributes.getValue("height"));
                rot = Float.parseFloat(attributes.getValue("rot"));
                gameObjects.add(Wall.makeBlueWall(x, y, width, height, rot, scaleFactor, universe));
                break;

            case "cyanWall": // makes a cyan wall
                x = Float.parseFloat(attributes.getValue("x"));
                y = Float.parseFloat(attributes.getValue("y"));
                width = Float.parseFloat(attributes.getValue("width"));
                height = Float.parseFloat(attributes.getValue("height"));
                rot = Float.parseFloat(attributes.getValue("rot"));
                gameObjects.add(Wall.makeCyanWall(x, y, width, height, rot, scaleFactor, universe));
                break;

            case "magentaWall": // makes a magenta wall
                x = Float.parseFloat(attributes.getValue("x"));
                y = Float.parseFloat(attributes.getValue("y"));
                width = Float.parseFloat(attributes.getValue("width"));
                height = Float.parseFloat(attributes.getValue("height"));
                rot = Float.parseFloat(attributes.getValue("rot"));
                gameObjects.add(Wall.makeMagentaWall(x, y, width, height, rot, scaleFactor, universe));
                break;

            case "yellowWall": // makes a yellow wall
                x = Float.parseFloat(attributes.getValue("x"));
                y = Float.parseFloat(attributes.getValue("y"));
                width = Float.parseFloat(attributes.getValue("width"));
                height = Float.parseFloat(attributes.getValue("height"));
                rot = Float.parseFloat(attributes.getValue("rot"));
                gameObjects.add(Wall.makeYellowWall(x, y, width, height, rot, scaleFactor, universe));
                break;

            case "whiteCircle": // makes a white circle
                x = Float.parseFloat(attributes.getValue("x"));
                y = Float.parseFloat(attributes.getValue("y"));
                gameObjects.add(Circle.makeWhiteCircle(x, y, scaleFactor, soundPlayer, universe));
                break;

            case "redCircle": // makes a red circle
                x = Float.parseFloat(attributes.getValue("x"));
                y = Float.parseFloat(attributes.getValue("y"));
                gameObjects.add(Circle.makeRedCircle(x, y, scaleFactor, soundPlayer, universe));
                break;

            case "greenCircle": // makes a green circle
                x = Float.parseFloat(attributes.getValue("x"));
                y = Float.parseFloat(attributes.getValue("y"));
                gameObjects.add(Circle.makeGreenCircle(x, y, scaleFactor, soundPlayer, universe));
                break;

            case "blueCircle": // makes a blue circle
                x = Float.parseFloat(attributes.getValue("x"));
                y = Float.parseFloat(attributes.getValue("y"));
                gameObjects.add(Circle.makeBlueCircle(x, y, scaleFactor, soundPlayer, universe));
                break;

            case "cyanCircle": // makes a cyan circle
                x = Float.parseFloat(attributes.getValue("x"));
                y = Float.parseFloat(attributes.getValue("y"));
                gameObjects.add(Circle.makeCyanCircle(x, y, scaleFactor, soundPlayer, universe));
                break;

            case "magentaCircle": // makes a magenta circle
                x = Float.parseFloat(attributes.getValue("x"));
                y = Float.parseFloat(attributes.getValue("y"));
                gameObjects.add(Circle.makeMagentaCircle(x, y, scaleFactor, soundPlayer, universe));
                break;

            case "yellowCircle": // makes a yellow circle
                x = Float.parseFloat(attributes.getValue("x"));
                y = Float.parseFloat(attributes.getValue("y"));
                gameObjects.add(Circle.makeYellowCircle(x, y, scaleFactor, soundPlayer, universe));
                break;

            case "player": // makes the player
                x = Float.parseFloat(attributes.getValue("x"));
                y = Float.parseFloat(attributes.getValue("y"));
                gameObjects.add(new Player(2, scaleFactor, universe, x, y,0, window, keyHandler, soundPlayer));
                playerIndex = gameObjects.size()-1;
                ((Player)gameObjects.get(playerIndex)).levelWidth = levelWidth;
                break;

            case "level": // sets level properties
                universe = new Universe(new Vector2(Float.parseFloat(attributes.getValue("gravityX")), Float.parseFloat(attributes.getValue("gravityY"))));
                levelWidth = Float.parseFloat(attributes.getValue("finishWidth"));
                break;
        }

    }

    public void run(){ // called every frame level is used

        if(!((Player)gameObjects.get(playerIndex)).getIsAlive()){   /* if the player is dead reset the level and clear
                                                                    player effects/force/torque*/
            reset();
            gameObjects.get(playerIndex).clearForce();
            gameObjects.get(playerIndex).clearTorque();
            ((Player) gameObjects.get(playerIndex)).clearEffects();
        }
        //this is first so that the player can be shown as dead for a frame

        for(GameObject gameObject : gameObjects){ // iterates through all gameObjects and runs/renders them
            gameObject.tick();
            gameObject.render(window);
        }

        if(gameObjects.get(playerIndex).getPosition().x > levelWidth*scaleFactor)isLevelPassed = true; // if the player has won set isLevelPassed to true

        steps = (float)((glfwGetTime()-lastPhysUpdate)*670);

        if(steps > 1) {
            universe.step((int) steps); // step the physics forward by elapsed time
        }else{
            accumSteps += steps;

            if(accumSteps > 1){
                universe.step((int)accumSteps);
                accumSteps = 0;
            }
        }

        lastPhysUpdate = (float)glfwGetTime();

    }

    private void reset(){ // reset level
        try {
            gameObjects = new ArrayList<>();

            File level = new File(path);

            SAXParserFactory factory = SAXParserFactory.newInstance();

            SAXParser sax = factory.newSAXParser();

            sax.parse(level, this);

            window.camera.translate(-window.camera.getX(), -window.camera.getY());

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public boolean getIsLevelPassed(){
        return isLevelPassed;
    } // returns isLevelPassed

    public boolean getIsPlayerAlive(){return ((Player)gameObjects.get(playerIndex)).getIsAlive();}

}
