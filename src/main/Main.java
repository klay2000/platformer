package main;

import Sound.*;
import graphics.Quad;
import graphics.Texture;
import graphics.Window;
import org.lwjgl.openal.AL;
import org.lwjgl.openal.ALC;
import org.lwjgl.openal.ALCCapabilities;
import org.lwjgl.openal.ALCapabilities;

import javax.xml.stream.events.StartDocument;
import java.awt.*;
import java.io.File;
import java.nio.IntBuffer;
import java.nio.ShortBuffer;

import static org.lwjgl.glfw.GLFW.*;
import static org.lwjgl.openal.AL10.*;
import static org.lwjgl.openal.ALC10.*;
import static org.lwjgl.stb.STBVorbis.stb_vorbis_decode_filename;
import static org.lwjgl.system.MemoryStack.stackMallocInt;
import static org.lwjgl.system.MemoryStack.stackPop;
import static org.lwjgl.system.MemoryStack.stackPush;
import static org.lwjgl.system.libc.LibCStdlib.free;

public class Main {

    public static void main(String args[]){

        int width, height;

        width = 1920;
        height = 1080;

        int levelNum;
        int levels = 20;

        float maxFrameRate = GraphicsEnvironment.getLocalGraphicsEnvironment().getDefaultScreenDevice().getDisplayMode().getRefreshRate();
        float lastSleepTime = 0;

        float scaleFactor = 0.1f;

        int lives;

        KeyboardHandler keyHandler = new KeyboardHandler();

        Window window = new Window(width, height, keyHandler,"Platformer", false);
        window.camera.zoom(1/scaleFactor);

        SoundPlayer soundPlayer = new SoundPlayer();

        float timeOfDeathScreenShown = 0;
        float deathScreenShownLength = 5;

        //load from save if possible, otherwise make a save
        Save save = new Save();
        if (new File("save.sav").exists()) save.load("save.sav");
        else save.save("save.sav");
        lives = save.lives;
        levelNum = save.level;
        if(lives < 0){
            save.level = 1;
            save.lives = 3;
            lives = 3;
            levelNum = 1;
        }

        int state = 0;

        Texture loading = new Texture("res/loadingScreen.png");
        Texture loadingIcon = new Texture("res/loadingIcon.png");

        while(!Resources.done) {
            window.queueModel(new Quad(width*scaleFactor/2, height*scaleFactor/2, width*scaleFactor, height*scaleFactor, loading));

            Quad icon = new Quad(0, 0, 100*scaleFactor,100*scaleFactor, loadingIcon);

            icon.translate(width*scaleFactor/2, height*scaleFactor/2);

            icon.rotate(Resources.assetsLoaded*25);

            window.queueModel(icon);

            window.drawFrame();

            Resources.loadPartial();
        }

        Level level = new Level("res/levels/level"+levelNum+".xml", scaleFactor, window, keyHandler, soundPlayer);

        Thread music = new Thread(new MusicPlayer(soundPlayer)); // make thread to play music

        music.start(); // start the music thread

        while(true) {

            switch(state){

                case 0: // main menu

                    window.camera.translate(-window.camera.getX(), -window.camera.getY());

                    state += Menu.mainMenu(window, scaleFactor, keyHandler, soundPlayer);

                    break;

                case 1: // game running
                level.run();

                // check if player is dead and adjust stuff accordingly
                if (!level.getIsPlayerAlive()) {
                    lives--;
                    save.lives = lives;
                    save.save("save.sav");
                }

                //if esc down go to menu
                if(keyHandler.getKey(GLFW_KEY_ESCAPE)) state = 0;

                //draw UI
                for (int i = 0; i < lives; i++) {
                    float lifeSize = 5;
                    window.queueModel(new Quad(((0 + lifeSize / 2) + (lifeSize + 1)) * (i + 1) - window.camera.getX(), 100 - window.camera.getY(), lifeSize, lifeSize, Resources.whiteCircle));
                }

                if (lives < 0){ // show death screen when player runs out of lives
                    state = 2;
                    timeOfDeathScreenShown = (float) glfwGetTime();
                }


                if (level.getIsLevelPassed()) {
                    levelNum++;
                    if(levelNum <= levels) {
                        level = new Level("res/levels/level" + levelNum + ".xml", scaleFactor, window, keyHandler, soundPlayer);
                        save.level = levelNum;
                        save.save("save.sav");
                    }
                    else{
                        window.camera.translate(-window.camera.getX(), -window.camera.getY());
                        window.queueModel(new Quad(width/2*scaleFactor, height/2*scaleFactor, width*scaleFactor, height*scaleFactor, Resources.winScreen));
                        window.drawFrame();
                        save.level = 1;
                        save.lives = 3;
                        levelNum = save.level;
                        level = new Level("res/levels/level1.xml", scaleFactor, window, keyHandler, soundPlayer);
                        save.save("save.sav");
                        state = 0;
                        try {
                            Thread.sleep(5000);
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                    }
                }
                break;

                case 2:

                    // draw death screen
                    window.camera.translate(-window.camera.getX(), -window.camera.getY());

                    window.queueModel(new Quad(width/2*scaleFactor, height/2*scaleFactor, width*scaleFactor, height*scaleFactor, Resources.deathScreen));

                    if(glfwGetTime() - timeOfDeathScreenShown > deathScreenShownLength){ // reset everything and go to menu after screen has been shown

                        lives = 3;
                        levelNum = 1;
                        level = new Level("res/levels/level1.xml", scaleFactor, window, keyHandler, soundPlayer);

                        save.lives = 3;
                        save.level = 1;
                        save.save("save.sav");

                        state = 0;
                    }

                    break;

            }

            // stuff that should happen regardless of state

            window.drawFrame(); // updates window

            try { // adjusts delay to maintain frame rate
                float timeSinceSleep = (float) glfwGetTime() * 1000 - lastSleepTime;

                float maxFrameTime = 1000 / maxFrameRate;

                if (((maxFrameTime) - (timeSinceSleep)) >= 0)
                    Thread.sleep((long) ((maxFrameTime) - (timeSinceSleep)));

                lastSleepTime = (float) glfwGetTime() * 1000;
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

        }
    }

}
