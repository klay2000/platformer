package main;

import graphics.Texture;

import Sound.Sound;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class Resources {

    public static Texture whiteCircle;
    public static Texture redCircle;
    public static Texture greenCircle;
    public static Texture blueCircle;
    public static Texture cyanCircle;
    public static Texture magentaCircle;
    public static Texture yellowCircle;

    public static Texture whiteWall;
    public static Texture redWall;
    public static Texture greenWall;
    public static Texture blueWall;
    public static Texture cyanWall;
    public static Texture magentaWall;
    public static Texture yellowWall;

    public static Texture menuBackground;
    public static Texture deathScreen;
    public static Texture winScreen;

    public static Sound switchItem;
    public static Sound circleCollide;

    public static ArrayList<Sound> music = new ArrayList<>();

    public static int assetsLoaded = 0;

    private static ArrayList<String> objects = new ArrayList<>();

    public static boolean done = false;

    public static void loadPartial(){ //loads a portion of the assets every time its called
        if(assetsLoaded == 0) {
            whiteCircle = new Texture("res/circle.png");
            redCircle = new Texture("res/redCircle.png");
            greenCircle = new Texture("res/greenCircle.png");
            blueCircle = new Texture("res/blueCircle.png");
            cyanCircle = new Texture("res/cyanCircle.png");
            magentaCircle = new Texture("res/magentaCircle.png");
            yellowCircle = new Texture("res/yellowCircle.png");

            whiteWall = new Texture("res/wall.png");
            redWall = new Texture("res/redWall.png");
            greenWall = new Texture("res/greenWall.png");
            blueWall = new Texture("res/blueWall.png");
            cyanWall = new Texture("res/cyanWall.png");
            magentaWall = new Texture("res/magentaWall.png");
            yellowWall = new Texture("res/yellowWall.png");

            menuBackground = new Texture("res/menuBG.png");
            deathScreen = new Texture("res/deathScreen.png");
            winScreen = new Texture("res/winScreen.png");

            switchItem = new Sound("res/soundEffects/switch.ogg", 1);
            circleCollide = new Sound("res/soundEffects/collide.ogg", 1);

            assetsLoaded++;
            return;
        }else {
            // read data for and load entire sound track
            try {

                if(assetsLoaded == 1) {
                    BufferedReader reader = new BufferedReader(new FileReader("res/soundTrack/tracks.txt"));

                    String line;

                    while ((line = reader.readLine()) != null) {
                        for (String object : line.split(",")) {
                            objects.add(object);
                        }
                    }
                    assetsLoaded++;
                }else {

                    while ((assetsLoaded - 2)*2 < objects.size()) {
                        music.add(new Sound("res/soundTrack/" + objects.get((assetsLoaded-2)*2), Integer.parseInt(objects.get(((assetsLoaded-2)*2)+1))));
                        assetsLoaded++;
                        if((assetsLoaded - 2)*2 < objects.size())return;
                    }

                    done = true;
                }
            } catch (IOException e) {
                e.printStackTrace();
            }

        }
    }

}
