package main;

import Sound.SoundPlayer;
import com.sun.org.apache.regexp.internal.RE;
import graphics.Quad;
import graphics.Window;

import static org.lwjgl.glfw.GLFW.*;
import static org.lwjgl.glfw.GLFW.glfwGetTime;

public class Menu {

    static int selected = 0; // menu item selected
    static float lastMenuItemSwitch = (float) glfwGetTime(); // time since last switch of menu item
    static float menuItemSwitchDelay = 0.25f; // minimum time between menu item switches

    public static int mainMenu(Window window, float scaleFactor, KeyboardHandler keyHandler, SoundPlayer soundPlayer){

        if (keyHandler.getKey(GLFW_KEY_ENTER) && glfwGetTime() - lastMenuItemSwitch > menuItemSwitchDelay) { // checks selected menu item and does stuff
            if (selected == 0) {
                selected = 0;
                return 1;
            }
            if (selected == 1){
                soundPlayer.cleanUp();
                System.exit(0);
            }
        }


        // below code changes selected menu item
        if (keyHandler.getKey(GLFW_KEY_S) && glfwGetTime() - lastMenuItemSwitch > menuItemSwitchDelay) {
            selected++;
            lastMenuItemSwitch = (float) glfwGetTime();
            if (selected > 1) selected = 0;
            soundPlayer.play(Resources.switchItem); // plays sound when item switched
        }
        if (keyHandler.getKey(GLFW_KEY_W) && glfwGetTime() - lastMenuItemSwitch > menuItemSwitchDelay) {
            selected--;
            lastMenuItemSwitch = (float) glfwGetTime();
            if (selected < 0) selected = 1;
            soundPlayer.play(Resources.switchItem); // plays sound when item switched
        }

        // draws menu screen
        window.queueModel(new Quad((window.getWidth() * scaleFactor) / 2, (window.getHeight() * scaleFactor) / 2, window.getWidth() * scaleFactor, window.getHeight() * scaleFactor, Resources.menuBackground));

        switch (selected) { // draws icon showing selected menu item
            case 0: // play
                window.queueModel(new Quad(860 * scaleFactor, 820 * scaleFactor, 80 * scaleFactor, 80 * scaleFactor, Resources.redCircle));
                break;

            case 1: // exit
                window.queueModel(new Quad(867 * scaleFactor, 744 * scaleFactor, 80 * scaleFactor, 80 * scaleFactor, Resources.greenCircle));
                break;

        }

        return 0;

    }


}
